import {ADD_DISHES} from "../actions/actionTypes";

const initialState = {
  dishesCart: [],
  disabled: true,
  totalPrice: 150
};

const cartReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_DISHES:
      return {
        ...state,
        dishesCart: {
          ...state.dishesCart,
          dishes: action.dishesName,
          price: action.price,
          totalPrice: state.totalPrice + action.price,
      },
        disabled: false
      };
    default:
      return state;
  }
};
export default cartReducer;