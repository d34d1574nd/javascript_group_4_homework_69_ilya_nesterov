import {DISHES_FAILURE, DISHES_REQUEST, DISHES_SUCCESS} from "../actions/actionTypes";

const initialState = {
  title: '',
  loading: false,
  error: null
};

const dishesReducer = (state = initialState, action) => {
  switch (action.type) {
    case DISHES_REQUEST:
      return {
        ...state,
        loading: true
      };
    case DISHES_SUCCESS:
      return {
        ...state,
        title: action.item,
        loading: false,
      };
    case DISHES_FAILURE:
      return {
        ...state,
        error: action.error
      };
    default:
      return state;
  }
};

export default dishesReducer;