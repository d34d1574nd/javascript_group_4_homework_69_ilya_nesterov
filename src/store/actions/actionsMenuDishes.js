import axios from '../../axiosBase';

import {DISHES_FAILURE, DISHES_REQUEST, DISHES_SUCCESS} from "./actionTypes";

export const dishesRequest = () => {
  return {type: DISHES_REQUEST};
};

export const dishesSuccess = item => {
  return {type: DISHES_SUCCESS, item};
};

export const dishesFailure = error => {
  return {type: DISHES_FAILURE, error};
};

export const fetchDishes = () => {
  return (dispatch, getState) => {
    dispatch(dishesRequest());
    axios.get('/dishes.json').then(response => {
      dispatch(dishesSuccess(response.data));
    }, error => {
      dispatch(dishesFailure(error));
    });
  }
};