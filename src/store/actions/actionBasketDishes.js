import {ADD_DISHES} from "./actionTypes";

export const addDishes = (dishesName, price) => {
  return {type: ADD_DISHES, dishesName, price};
};