import React, { Component } from 'react';

import './App.css';
import Restaurant from "./containers/restaurant/restaurant";

class App extends Component {
  render() {
    return (
      <div className="App">
        <Restaurant/>
      </div>
    );
  }
}

export default App;
