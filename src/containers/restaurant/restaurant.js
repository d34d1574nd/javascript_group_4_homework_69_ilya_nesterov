import React, {Component} from 'react';

import './restaurant.css';

import MenuDishes  from '../../components/menuDishes/menuDishes';
import BasketDishes  from '../../components/basketDishes/basketDishes';

class Restaurant extends Component {
  render() {
    return (
      <div className='restaurant'>
        <MenuDishes/>
        <BasketDishes/>
      </div>
    );
  }
}

export default Restaurant;