import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://counter-b1c4e.firebaseio.com/'
});

export default instance;