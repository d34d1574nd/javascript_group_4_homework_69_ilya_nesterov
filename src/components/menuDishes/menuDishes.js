import React, {Component} from 'react';
import {connect} from "react-redux";
import {fetchDishes} from "../../store/actions/actionsMenuDishes";
import {addDishes} from "../../store/actions/actionBasketDishes";

import './menuDishes.css';

import Spinner from "../UI/Spinner/Spinner";

class Dishes extends Component {
  componentDidMount() {
    this.props.loadData();
  }

  render() {
    let dishes = Object.keys(this.props.title).map((id) => (
      <div key={id} className='dishes'>
        <div className="image">
          <img src={this.props.title[id].image} alt={this.props.title[id].image}/>
        </div>
        <div className='text'>
          <p>{this.props.title[id].title}</p>
          <p>{this.props.title[id].price} <strong>KGS</strong></p>
        </div>
        <div className='button'>
          <button onClick={() => this.props.addDishes(
            this.props.title[id].title,
            this.props.title[id].price
          )}>
            <i className="fas fa-cart-arrow-down"/> Add to cart
          </button>
        </div>
      </div>
    ));
    return (
        <div className='menu'>
          {this.props.loading ? <Spinner/> : dishes}
        </div>
    );
  }
}

const mapStateToProps = state => ({
  title: state.dish.title,
  loading: state.dish.loading
});

const mapDispatchToProps = dispatch => ({
  loadData: () => dispatch(fetchDishes()),
  addDishes: (dishesName, price) => dispatch(addDishes(dishesName, price)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Dishes);