import React, {Component} from 'react';

import './basketDishes.css';

import {connect} from "react-redux";

class BasketDishes extends Component {
  render() {
    return (
      <div className='basket'>
        <p className='cart'>Basket Dishes:</p>
        <p>{this.props.dishes.dishes} <strong>{this.props.dishes.price}</strong></p>
        <p className='delivery'>Доставка:  150<span>KGS</span></p>
        <p className='total'>Итого: <span>KGS</span></p>
        <button className='btnPlace' disabled={this.props.disabled}>Place order</button>
      </div>
    );
  }
}

const mapStateToProps = state => ({
  dishes: state.cart.dishesCart,
  disabled: state.disabled,
});

const mapDispatchToProps = dispatch => ({

});

export default connect(mapStateToProps, mapDispatchToProps)(BasketDishes);